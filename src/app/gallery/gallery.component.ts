import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../photos.service'
import { Photo } from '../photo'

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  //config variables
  imagesCount = 5;
  imagesCountArray = Array(this.imagesCount).fill(0).map((x,i)=>i);
  currPhotoIndex = 0;

  //variables
  photosIsLoaded = false;
  photos:Photo[];
  photosToShow:Photo[];
  currentPhoto:Photo;
  
  constructor(private photosService: PhotosService) { }

  ngOnInit() {
   this.initApp();
  }
  
  initApp(){
   this.photosService.getPhotos()
   .subscribe(photos=>{
     this.photos = photos; 
     this.photosIsLoaded = true;
     this.takePhotosToShow();

    }); 
  }

  takePhotosToShow(){
    this.photosToShow = [];
    for (let index = 0; index < this.imagesCount; index++) {
      this.photosToShow.push(this.photos[index]);
    }
    this.photos.splice(0,this.imagesCount)
    this.setCurrentPhoto(this.currPhotoIndex)
  }

  setCurrentPhoto(index:number){
    this.currPhotoIndex = index;
    this.currentPhoto = this.photosToShow[this.currPhotoIndex];
  }
}
