import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';


import { Photo } from './photo';
@Injectable()
export class PhotosService {

  dataUrl = '../assets/photosArray.json';

  constructor(private http: HttpClient) { }

  getPhotos(): Observable<Photo[]>{
    return this.http.get<Photo[]>(this.dataUrl)
    .pipe(catchError(this.handleError('getPhotos',[])));
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
