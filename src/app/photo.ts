export interface Photo {
    id:number;
    img:string;
    title:string;
    description:string;
}
